//Unidad de gestión de base de datos
//{$ModeSwitch UnicodeStrings}

unit DatabaseUtils;
{$mode objfpc}{$H+}

interface
const
  MAX_CHAR_NOMBRE = 32;
  MAX_CHAR_DESCRIPCION = 128;
  MAX_CHAR_USERNAME = 24;
  MAX_CHAR_NOMBRECOMPLETO = 64;
  MAX_CHAR_PASSWORD = 128;
  MAX_CHAR_EMAIL = 32;
  MAX_CHAR_TELEFONO = 9;
  MAX_PEDIDOS = 5;
  FECHA_LONGITUD_CHAR = 10;
type
  pedido_t = record
    monto : single;
    fecha : string[FECHA_LONGITUD_CHAR];
  end;
  arrayListaPedidos_t = array[1..MAX_PEDIDOS] of pedido_t;
  listaPedidos_t = record
    numeroPedidos : longint;
    pedidos : arrayListaPedidos_t;
  end;

  cliente_t = record
    username : string[MAX_CHAR_USERNAME];
    nombreCompleto : string[MAX_CHAR_NOMBRECOMPLETO];
    password : string[MAX_CHAR_PASSWORD];
    email : string[MAX_CHAR_EMAIL];
    telefono : string[MAX_CHAR_TELEFONO];
    pedidos : listaPedidos_t;
  end;
  producto_t = record
    nombre : string[MAX_CHAR_NOMBRE];
    descripcion : string[MAX_CHAR_DESCRIPCION];
    precio : single;
  end;
  metadatos_t = record
    numeroProductos : longint;
    numeroClientes : longint;
  end;
  database_t = record
    metadatos : metadatos_t;
    productos : array[0..4095] of producto_t;
    clientes : array[0..4095] of cliente_t;
  end;



procedure AbrirOCrearDatabase(var db : database_t; var ruta : string);
function CrearNuevaDatabase(ruta : string) : boolean;
procedure VerCarta(var db : database_t);
function HashPassword(pass : string) : string;
function __ArchivoExisteInternal(ruta : string) : boolean;
procedure __NuevoProductoInternal(nuevo : producto_t; var db : database_t; ruta : string);
procedure __ModificarProductoInternal(var db : database_t; ruta : string; indice : integer);
procedure LeerDatabase(var db : database_t; ruta : string);
procedure GuardarDatabase(var db : database_t; ruta : string);

implementation
uses classes, md5, sysutils, fileutil, strutils, crt, textutils;

function __ArchivoExisteInternal(ruta : string) : boolean;
var
  archivo : file;
begin
  assign(archivo, ruta);
  {$i-}
  reset(archivo);
  {$i+}
  if (IOResult = 0) then
  begin
    __ArchivoExisteInternal := true;
    closefile(archivo);
  end else
   __ArchivoExisteInternal := false;
end;

function CrearNuevaDatabase(ruta : string) : boolean;
begin
  if (__ArchivoExisteInternal(ruta) = false) then
  begin
    CrearNuevaDatabase := true;
  end else
    CrearNuevaDatabase := false;
end;

procedure LeerDatabase(var db : database_t; ruta : string);
var
  db_file : file of database_t;
begin
  assign(db_file, ruta);
  {$i-}
  reset(db_file);
  {$i+}
  if (IOResult = 0) then
  begin
    reset(db_file);
    read(db_file, db);
    close(db_file);
  end;
end;

function HashPassword(pass : string) : string;
begin
  //MD5 fué roto hace años
  //No debe ser usado en situaciones reales
  HashPassword := MD5Print(MD5String(pass));
end;

procedure GuardarDatabase(var db : database_t; ruta : string);
var
  db_file : file of database_t;
begin
  assign(db_file, ruta);
  rewrite(db_file);
  write(db_file, db);
  close(db_file);
end;

procedure AbrirOCrearDatabase(var db : database_t; var ruta : string);
var
  i, op : integer;
  exito : boolean;
  encontradas : TStringList;
  nuevaRuta : string;
begin
  encontradas := FindAllFiles(GetCurrentDir(), '*.db', false);
  repeat
    writeln('0 -> Crear nueva base de datos');
    for i := 0 to encontradas.Count-1 do writeln(i+1, ' -> ', encontradas[i]);
    write('Seleccione la acción a realizar: ');
    readln(op);
  until ((op >= 0) AND (op <= encontradas.Count));
  if (op = 0) then
  begin
    repeat
    begin
      write('Introduzca el nombre de la nueva base de datos, con la extensión .db: ');
      readln(nuevaRuta);
      if (rightstr(nuevaRuta, 3) <> '.db') then nuevaRuta := concat(nuevaRuta, '.db');
      writeln('Se creará una nueva base de datos en el directorio actual con el nombre de ', nuevaRuta, ' ...');
      exito := CrearNuevaDatabase(nuevaRuta);
      if (exito) then
      begin
        writeln('La base de datos se ha creado correctamente');
        ruta := nuevaRuta;
      end else
        writeln('No se ha podido crear la base de datos especificada. Por favor, compruebe que no ha introducido caracteres inválidos o que ya existe una base de datos con el mismo nombre');
    end;
    until (exito = true);
  end else ruta :=encontradas[op-1];
  LeerDatabase(db, ruta);
end;

procedure VerCarta(var db : database_t);
var
  i : integer;
begin
  ClrScr();
  writeln('Mostrando todos los productos');
  for i := 0 to db.metadatos.numeroProductos-1 do writeln(i, '. ', db.productos[i].nombre, ':', db.productos[i].descripcion);
  writeln('Presione cualquier tecla para salir de la vista...');
  readkey();
end;

function RecalcularMonto(carrito : array of producto_t; numProductos : integer) : single;
var
  i : integer;
begin
  RecalcularMonto := 0;
  for i := 0 to numProductos-1 do RecalcularMonto := RecalcularMonto + carrito[i].precio;
end;

procedure __NuevoProductoInternal(nuevo : producto_t; var db : database_t; ruta : string);
begin
  Inc(db.metadatos.numeroProductos);
  //setLength(db.productos, db.metadatos.numeroProductos);
  db.productos[db.metadatos.numeroProductos-1] := nuevo;
  GuardarDatabase(db, ruta);
end;

procedure __ModificarProductoInternal(var db : database_t; ruta : string; indice : integer);
var
  nuevoNombre : string[MAX_CHAR_NOMBRE];
  nuevaDescripcion : string[MAX_CHAR_DESCRIPCION];
  nuevoPrecioStr : string[12];
begin
  writeln('No introduzca nada si no quiere modificar el campo');
  writeln('Modificando producto ', indice, '...');

  write('Nombre: ', db.productos[indice].nombre, '. Nuevo nombre: ');
  readln(nuevoNombre);
  if (nuevoNombre <> '') then
    db.productos[indice].nombre := nuevoNombre;

  write('Descripción: ', db.productos[indice].descripcion, '. Nueva descripción: ');
  readln(nuevaDescripcion);
  if (nuevaDescripcion <> '') then
    db.productos[indice].descripcion:=nuevaDescripcion;

  write('Precio: ', db.productos[indice].precio:0:3, '. Nuevo precio: ');
  readln(nuevoPrecioStr);
  if (nuevoPrecioStr <> '') then
    db.productos[indice].precio:= StrToFloat(replacestr(nuevoPrecioStr, '.', ','));
  GuardarDatabase(db, ruta);
end;

end.
