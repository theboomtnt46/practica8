unit TextUtils;

{$mode objfpc}{$H+}

interface
uses classes, sysutils, strutils, crt, keyboard, dos;

procedure EscribirFormateado(str : string; highlightFlag, flag : integer; colorTexto : integer = black; colorFondo : integer = white);
function GetDateStr() : string;
function PasswordInput(longitudMax : integer) : string;

implementation

procedure EscribirFormateado(str : string; highlightFlag, flag : integer; colorTexto : integer = black; colorFondo : integer = white);
begin
  if (highlightFlag = flag) then
  begin
    TextColor(colorTexto);
    TextBackground(colorFondo);
  end;
  writeln(str);
  TextColor(lightgray);
  TextBackground(black);
end;

function PasswordInput(longitudMax : integer) : string;
var
  op : char;
  contador : integer;
  origX, origY : tcrtcoord;
  pass : string;
begin
  pass := '';
  contador := 0;
  origX := WhereX();
  origY := WhereY();
  repeat
    GotoXY(origX, origY);
    write(addchar('*', '', contador));
    op := readkey();
    if ((ord(op) > 32) and (ord(op) < 127) and (contador < longitudMax)) then
    begin
      pass := concat(pass, op);
      inc(contador);
    end;
    if ((op = #8) and (contador > 0)) then
    begin
      pass := leftstr(pass, length(pass)-1);
      dec(contador);
    end;
    GotoXY(origX, WhereY());
    write(addchar(' ', '', longitudMax));
  until (op = #13);
  PasswordInput := pass;
end;

function GetDateStr() : string;
var
  dia, mes, year, aux : word;
begin
  getdate(year, mes, dia, aux);
  GetDateStr := concat(IntToStr(year), '-', InttoStr(mes), '-', IntToStr(dia));
end;

end.

