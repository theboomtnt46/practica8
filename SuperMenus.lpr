//{$ModeSwitch UnicodeStrings}
program SuperMenus_Admin;
uses databaseutils, textutils, crt, classes, windows, fileutil, sysutils;

var
  opcion : integer;
  ruta_db : string;

  database : database_t;
  archivo_db : text;

function MostrarMenu : integer;
var
  selector : integer;
  op : char;
begin
  selector := -1;
  repeat
    ClrScr();
    writeln('Use las teclas de direccion Arriba/Abajo para navegar por los menús');
    writeln('-------------------------------------------------------------------');
    EscribirFormateado('SALIR del programa', -1, selector);
    EscribirFormateado('Añadir producto', 0, selector);
    EscribirFormateado('Modificar producto', 1, selector);
    EscribirFormateado('Mostrar todos los productos', 2, selector);
    EscribirFormateado('Ver listado de clientes', 3, selector);
    EscribirFormateado('Borrar cliente', 4, selector);

    op := ReadKey();
    case (op) of
      #72: if (selector > -1) then Dec(selector);
      #80: if (selector < 4) then Inc(selector);
    end;
  until ((op = #13) OR (op = #77));
  MostrarMenu := selector;
end;

procedure MostrarClientes(var db : database_t);
var
  i, j : integer;
begin
  ClrScr();
  for i := 0 to db.metadatos.numeroClientes-1 do
  begin
    writeln(i, '. ---------------------------------');
    writeln('Nombre completo: ', db.clientes[i].nombreCompleto);
    writeln('Nombre de usuario: ', db.clientes[i].username);
    writeln('Correo electrónico: ', db.clientes[i].email);
    writeln('Contraseña (hasheada): ', db.clientes[i].password);
    writeln('Teléfono: ', db.clientes[i].telefono);
    writeln('Mostrando últimos 5 pedidos:');
    for j := 1 to db.clientes[i].pedidos.numeroPedidos do
      writeln('   Pedido ', j, ' a fecha de ', db.clientes[i].pedidos.pedidos[j].fecha, ' de coste ', db.clientes[i].pedidos.pedidos[j].monto:0:2, ' euros');
  end;
  write('Presione cualquier tecla para volver al menú principal...');
  readkey();
end;

procedure NuevoProducto(var db : database_t; ruta : string);
var
  nuevo : producto_t;
begin
  ClrScr();
  writeln('Se va a crear un nuevo producto');
  write('Nombre: ');
  readln(nuevo.nombre);
  write('Descripción: ');
  readln(nuevo.descripcion);
  write('Precio: ');
  readln(nuevo.precio);

  __NuevoProductoInternal(nuevo, db, ruta)
end;

procedure __ModificarProductoPorIndiceInternal(var db : database_t; ruta : string);
var
  indice : integer;
begin
  write('Introduzca el índice del producto: ');
  readln(indice);
  if ((indice < db.metadatos.numeroProductos) AND (indice >= 0)) then
    __ModificarProductoInternal(db, ruta, indice)
  else begin
    EscribirFormateado('ERROR: Índice fuera de límites', 1, 1, black, red);
    readln;
  end;
end;

procedure __ModificarProductoPorNombreInternal(var db : database_t; ruta : string);
var
  nombre : string;
  i, x : integer;
  op : char;
  listaAux : array[0..15] of integer;
begin
  i := 0;
  write('Introduzca el nombre del producto a modificar: ');
  readln(nombre);
  while ((db.productos[i].nombre <> nombre) AND (i < db.metadatos.numeroProductos)) do Inc(i);
  if (db.productos[i].nombre = nombre) then
  begin
    writeln('Se ha encontrado el producto en la posición ', i);
    __ModificarProductoInternal(db, ruta, i);
  end else
  begin
    write('No se ha encontrado el producto. ¿Desea buscar entre productos similares? Ss/Nn: ');
    readln(op);
    if ((op = 'S') OR (op = 's')) then
    begin
      x := 0;
      for i := 0 to db.metadatos.numeroProductos-1 do
      begin
        if (pos(lowercase(nombre), lowercase(db.productos[i].nombre)) <> 0) then
        begin
          Inc(x);
          listaAux[x]:=i;
        end;
      end;
      if (x <> 0) then
      begin
        writeln('Se han encontrado nombres similares. Se mostrarán a continuación.');
        writeln('0 -> No hacer nada');
        for i := 1 to x do writeln(i, ' -> producto ', listaAux[i], ' con nombre ', db.productos[listaAux[i]].nombre);
        write('Seleccione la acción a realizar: ');
        readln(i);
        if (i <> 0) then
          __ModificarProductoInternal(db, ruta, listaAux[i])
        else
          writeln('No se modificará nada');
      end else writeln('No se han encontrado productos similares');
    end;
  end;
end;

procedure __ModificarProductoPorListaInternal(var db : database_t; ruta : string);
var
  op : char;
  selector, i : integer;
begin
  selector := -1;
  repeat
    ClrScr();
    EscribirFormateado('VOLVER AL MENÚ PRINCIPAL', -1, selector);
    for i := 0 to db.metadatos.numeroProductos-1 do EscribirFormateado(db.productos[i].nombre, i, selector);

    op := ReadKey();
    case (op) of
      #72: if (selector > -1) then Dec(selector);
      #80: if (selector < db.metadatos.numeroProductos-1) then Inc(selector);
    end;
  until ((op = #13) OR (op = #77));
  if (selector <> -1) then __ModificarProductoInternal(db, ruta, selector);
end;

procedure ModificarProducto(var db : database_t; ruta : string);
var
  op : char;
  selector : integer;
begin
  selector := -1;
  repeat
    ClrScr();
    EscribirFormateado('VOLVER AL MENÚ PRINCIPAL', -1, selector);
    EscribirFormateado('Modificar por índice', 0, selector);
    EscribirFormateado('Modificar por nombre', 1, selector);
    EscribirFormateado('Modificar por lista', 2, selector);

    op := ReadKey();
    case (op) of
      #72: if (selector > -1) then Dec(selector);
      #80: if (selector < 2) then Inc(selector);
    end;
  until ((op = #13) OR (op = #77));
  case (selector) of
    0: __ModificarProductoPorIndiceInternal(db, ruta);
    1: __ModificarProductoPorNombreInternal(db, ruta);
    2: __ModificarProductoPorListaInternal(db, ruta);
  end;
end;

procedure BorrarCliente(db : database_t; ruta : string);
var
  op : char;
  i, c, selector : integer;
  hecho : boolean;
begin
  selector := -1;
  hecho := false;
  repeat
  repeat
  ClrScr();
  EscribirFormateado('VOLVER AL MENÚ PRINCIPAL', -1, selector);
  for i := 0 to db.metadatos.numeroClientes-1 do EscribirFormateado(db.clientes[i].username, i, selector);

  op := ReadKey();
  case (op) of
    #72: if (selector > -1) then dec(selector);
    #80: if (selector < db.metadatos.numeroClientes-1) then inc(selector);
  end;
  until ((op = #13) OR (op = #77));
  if (selector <> -1) then
  begin
    ClrScr();
    write('¿Realmente desea eliminar el usuario ', db.clientes[selector].username, '? (Ss/Nn)');
    case readkey() of
      #115, #83:
        begin
          c := 0;
          for i := 0 to db.metadatos.numeroClientes-1 do
          begin
            if (i <> selector) then
            begin
              db.clientes[c] := db.clientes[i];
              inc(c);
            end;
          end;
          dec(db.metadatos.numeroClientes);
          GuardarDatabase(db, ruta);
          writeln;
          writeln('Cliente borrado. Presione cualquier tecla para volver al menú...');
          readkey();
        end;
      #78, #110:
        begin
        hecho := true;
        writeln;
        writeln('Borrado cancelado. Presione cualquier tecla para volver al menú...');
        readkey();
        end;
    end;
  end else hecho := true;
  until hecho;
end;

begin
  AbrirOCrearDatabase(database, ruta_db);
  repeat
  ClrScr();
  opcion := MostrarMenu();
  case (opcion) of
    0: NuevoProducto(database, ruta_db);
    1: ModificarProducto(database, ruta_db);
    2: VerCarta(database);
    3: MostrarClientes(database);
    4: BorrarCliente(database, ruta_db);
  end;
  until (opcion = -1);
end.

