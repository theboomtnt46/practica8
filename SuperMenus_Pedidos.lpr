//Programa de usuario y gestión de pedidos

program SuperMenus_Pedidos;

uses textutils, windows, databaseutils, sysutils, crt, publi_store;
var
  sesion, opcion : integer;

  ruta_db : string;

  database : database_t;

procedure MostrarPublicidad;
var
  i : integer;
begin
  i := random(3);
  case (i) of
    0: begin
      for i := 1 to 3 do
      begin
        writeln(publi1);
        delay(500);
        ClrScr();
        writeln;
        writeln;
        writeln(publi1);
        delay(500);
        ClrScr();
        end;
      end;
    1: begin
      writeln(publi2_0);
      delay(750);
      ClrScr();
      writeln(publi2_1);
      delay(750);
      ClrScr();
      writeln(publi2_2);
      delay(750);
      ClrScr();
      for i := 1 to 5 do
      begin
        EscribirFormateado(publi2_3, 1, 1, black, white);
        delay(200);
        ClrScr();
        EscribirFormateado(publi2_3, 1, 1, white, black);
        delay(200);
        ClrScr();
      end;
    end;
    2: begin
      EscribirFormateado(publi3_0, 1, 1, red, black);
      delay(1000);
      ClrScr();
      EscribirFormateado(publi3_1, 1, 1, red, black);
      delay(1000);
      end;
  end;
end;

function MostrarMenu : integer;
var
    selector : integer;
    op : char;
begin
    MostrarPublicidad();

    selector := -1;
    repeat
      ClrScr();
      writeln('Use las teclas de direccion Arriba/Abajo para navegar por los menus');
      writeln('-------------------------------------------------------------------');
      EscribirFormateado('SALIR del programa', -1, selector);
      EscribirFormateado('Ver carta', 0, selector);
      EscribirFormateado('Hacer pedido', 1, selector);
      EscribirFormateado('Ver historico de pedidos', 2, selector);

      op := readkey();
      case (op) of
        #72: if (selector > -1) then Dec(selector);
        #80: if (selector < 2) then Inc(selector);
      end;
    until ((op = #13) OR (op = #77));
    MostrarMenu := selector;
end;

function Login(var db : database_t; var user_session : integer) : boolean;
var
  i, c : integer;
  user, pass : string;
begin
  Login := false;
  c := 0;
  repeat
  write('Nombre de usuario: ');
  readln(user);
  write('Contraseña: ');
  pass := PasswordInput(255);
  writeln;
  for i := 0 to db.metadatos.numeroClientes-1 do
  begin
    if ((user <> '') AND (db.clientes[i].username = user) AND (db.clientes[i].password = HashPassword(pass))) then
    begin
      Login := true;
      user_session := i;
    end;
  end;
  inc(c);
  if (Login = false) then
  begin
    EscribirFormateado('Credenciales incorrectas', 1, 1, black, red);
    delay(3*1000);
  end;
  until ((Login = true) OR (c = 3));
end;

function CrearCuenta(var db : database_t; ruta : string; var user_session : integer) : boolean;
var
  nuevo : cliente_t;
  pass1, pass2 : string;
  condicion : boolean;
  i : integer;
begin
  writeln('Se va a crear una nueva cuenta de cliente. Por favor, rellene los siguientes datos.');
  writeln;

  repeat
    condicion := true;
    write('Nombre de usuario: ');
    readln(nuevo.username);

    for i := 0 to db.metadatos.numeroClientes-1 do
    begin
      if (db.clientes[i].username = nuevo.username) then
      begin
        condicion := false;
        EscribirFormateado('Ya existe una cuenta con este nombre de usuario.', 1, 1, black, red);
      end;
    end;
  until condicion;
  repeat
    condicion := true;
    write('Correo electronico: ');
    readln(nuevo.email);

    for i := 0 to db.metadatos.numeroClientes-1 do
    begin
      if (db.clientes[i].email = nuevo.email) then
      begin
        condicion := false;
        EscribirFormateado('Ya existe una cuenta con este correo electronico.', 1, 1, black ,red);
      end;
    end;
  until condicion;
  repeat
  write('Contraseña: ');
  pass1 := PasswordInput(MAX_CHAR_PASSWORD);
  writeln;
  write('Repita la contraseña: ');
  pass2 := PasswordInput(MAX_CHAR_PASSWORD);
  writeln;
  until (pass1 = pass2);
  nuevo.password:=HashPassword(pass1);
  write('Nombre y apellidos: ');
  readln(nuevo.nombreCompleto);
  repeat
    condicion := true;
    write('Telefono: ');
    readln(nuevo.telefono);

    for i := 0 to db.metadatos.numeroClientes-1 do
    begin
      if (db.clientes[i].telefono = nuevo.telefono) then
      begin
        condicion := false;
        EscribirFormateado('Ya existe una cuenta con este telefono.' ,1, 1, black, red);
      end;
    end;
  until condicion;

  inc(db.metadatos.numeroClientes);
  db.clientes[db.metadatos.numeroClientes-1] := nuevo;
  GuardarDatabase(db, ruta);
  user_session := db.metadatos.numeroClientes-1;
  CrearCuenta := true;
end;

function PantallaInicio(var db : database_t; ruta : string; var user_session : integer) : boolean;
var
  op : char;
  selector : integer;
begin
  selector := -1;
  repeat
    ClrScr();
    EscribirFormateado('SALIR', -1, selector);
    EscribirFormateado('Iniciar sesion', 0, selector);
    EscribirFormateado('Crear nuevo usuario', 1, selector);

    op := readkey();
    case (op) of
      #72: if (selector > -1) then dec(selector);
      #80: if (selector < 1) then inc(selector);
    end;
  until ((op = #13) OR (op = #77));
  case (selector) of
    -1: PantallaInicio := false;
    0:
      begin
        if (Login(db, user_session) = true) then
        begin
          EscribirFormateado(concat('Iniciada sesion del usuario ', db.clientes[user_session].username), 1, 1, black, green);
          PantallaInicio := true;
          delay(500)
        end else begin
          EscribirFormateado('Ha fallado 3 veces. No se ha iniciado la sesion', 1, 1, black, red);
          PantallaInicio := false;
          delay(1500);
        end;
      end;
    1: PantallaInicio := CrearCuenta(db, ruta, user_session);
  end;
end;

procedure VerPedido(pedido : pedido_t);
begin
  writeln(concat('Fecha: ', pedido.fecha, ' -- Monto: ', FloatToStrF(pedido.monto, ffCurrency, 2, 2), '€'));
end;

procedure VerPedidos(var db : database_t; idCliente : integer);
var
  i : integer;
begin
  ClrScr();
  if (db.clientes[idCliente].pedidos.numeroPedidos > 0) then
  begin
  writeln('Mostrando los ultimos ', db.clientes[idCliente].pedidos.numeroPedidos ,' pedidos:');
  for i := 1 to db.clientes[idCliente].pedidos.numeroPedidos do VerPedido(db.clientes[idCliente].pedidos.pedidos[i]);
  writeln;
  end else writeln('Este usuario no ha hecho ningun pedido.');
  writeln('Presione cualquier tecla para volver al menu principal...');
  readkey();
end;

function RecalcularMonto(carrito : array of producto_t; numProductos : integer) : single;
var
  i : integer;
begin
  RecalcularMonto := 0;
  for i := 0 to numProductos-1 do RecalcularMonto := RecalcularMonto + carrito[i].precio;
end;



procedure ConfirmarPedido(var db : database_t; idCliente : integer; __carrito : array of producto_t; numProductos : integer; ruta : string);
var
  op : char;
  monto : single;
  i,c, selector : integer;
  carrito, carritoModificado : array of producto_t;
begin
  selector := -1;
  setLength(carrito, numProductos);
  for i:= 0 to numProductos-1 do carrito[i] := __carrito[i];

  monto := RecalcularMonto(carrito, numProductos);

  repeat
    repeat
      ClrScr();
      EscribirFormateado(concat('Total carrito: ', FloatToStrF(monto, ffCurrency, 2, 2), ' euros'), 1, 1, green, magenta);
      EscribirFormateado('Puede eliminar(-) o añadir una copia(+) de los productos de su pedido antes de terminar, seleccionando y pulsando las teclas indicadas.', 1, 1, 8, black);
      EscribirFormateado('CANCELAR PEDIDO', -2, selector);
      EscribirFormateado('FINALIZAR PEDIDO', -1, selector);
      writeln;

      for i := 0 to numProductos-1 do
        EscribirFormateado(concat(carrito[i].nombre, ': ', carrito[i].descripcion, ' --- ', FloatToStrF(carrito[i].precio, ffCurrency, 2, 2), '€'), i, selector);

      op := ReadKey();
      case (op) of
        #72: if (selector > -2) then dec(selector);
        #80: if (selector < numProductos-1) then Inc(selector);
        #45:
          begin
            if (selector > -1) then
            begin
              c := 0;
              setLength(carritoModificado, numProductos-1);
              for i := 0 to numProductos-1 do
              begin
                if (i <> selector) then
                begin
                  carritoModificado[c] := carrito[i];
                  inc(c);
                end;
              end;
              setLength(carrito, numProductos-1);
              dec(numProductos);
              carrito := carritoModificado;
            end;
          end;
        #43:
          begin
            setLength(carrito, numProductos+1);
            carrito[numProductos] := carrito[selector];
            inc(numProductos);
          end;
      end;
      monto := RecalcularMonto(carrito, numProductos);
      if (selector > numProductos-1) then selector := numProductos-1;
    until ((selector < 0) AND ((op = #13) OR (op = #77)));
    case (selector) of
      -2 :
        begin
          EscribirFormateado('Pedido cancelado. Volviendo al menu en 2 segundos...', 1, 1, black, red);
          delay(2*1000);
        end;
      -1:
        begin
          ClrScr();
          EscribirFormateado('Pedido confirmado. Presione cualquier tecla para volver al menu principal...', 1, 1, black, green);
          inc(db.clientes[idCliente].pedidos.numeroPedidos);
          db.clientes[idCliente].pedidos.pedidos[db.clientes[idCliente].pedidos.numeroPedidos MOD MAX_PEDIDOS].monto:=RecalcularMonto(carrito, numProductos);
          db.clientes[idCliente].pedidos.pedidos[db.clientes[idCliente].pedidos.numeroPedidos MOD MAX_PEDIDOS].fecha:=GetDateStr();
          GuardarDatabase(db, ruta);
          VerPedido(db.clientes[idCliente].pedidos.pedidos[db.clientes[idCliente].pedidos.numeroPedidos]);
          readkey();
        end;
    end;
  until (selector < 0);
end;

procedure HacerPedido(var db : database_t; datosCliente : integer; ruta : string);
var
  op : char;
  i, c, selector : integer;
  monto : single;
  carrito : array of producto_t;
begin
  c := 1;
  monto := 0;
  selector := -1;
  repeat
    setLength(carrito, c);
    repeat
      ClrScr();
      EscribirFormateado(concat('Total carrito: ', FloatToStrF(monto, ffCurrency, 2, 2), '€'), 1, 1, black, blue);
      EscribirFormateado('CANCELAR PEDIDO', -2, selector);
      EscribirFormateado('TRAMITAR PEDIDO', -1, selector);
      for i := 0 to db.metadatos.numeroProductos-1 do
        EscribirFormateado(concat(db.productos[i].nombre, ': ', db.productos[i].descripcion, ' --- ', FloatToStrF(db.productos[i].precio, ffCurrency, 2, 2), '€'), i, selector);

      op := readkey();
      case (op) of
        #72: if (selector > -2) then dec(selector);
        #80: if (selector < db.metadatos.numeroProductos-1) then inc(selector);
      end;
    until ((op = #13) OR (op = #77));
    case (selector) of
      -2 :
        begin
          EscribirFormateado('Pedido cancelado. Volviendo al menu en 2 segundos...', 1, 1, black, red);
          delay(2*1000);
        end;
      -1:
        begin
          ClrScr();
          ConfirmarPedido(db, datosCliente, carrito, c-1, ruta);
        end;
    end;
    if (selector > -1) then
    begin
      carrito[c-1] := db.productos[selector];
      inc(c);
      writeln('Se ha puesto ', db.productos[selector].nombre, ' en el carrito.');
      monto := monto + db.productos[selector].precio;
      write('Presione cualquier tecla para continuar...');
      readkey();
    end;
  until (selector < 0);
end;

begin
  randomize;
  SetConsoleOutputCP(CP_ACP);
  SetTextCodePage(Output, CP_ACP);
  //SetTextCodePage(Output, CP_UTF8);
  AbrirOCrearDatabase(database, ruta_db);
  if (PantallaInicio(database, ruta_db, sesion)) then
  begin
    repeat
    ClrScr();
    opcion := MostrarMenu();
    case (opcion) of
      0: VerCarta(database);
      1: HacerPedido(database, sesion, ruta_db);
      2: VerPedidos(database, sesion);
    end;
    until (opcion = -1);
  end;
end.
